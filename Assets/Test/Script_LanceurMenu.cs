﻿using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Script_LanceurMenu : MonoBehaviour
{
    public Button BtnModele;
    public Button BtnTestIA;
    public Button BtnVaisseau;
    public Button BtnVersion3D;
    public Button BtnPlateforme;
    public Button BtnFlappy;
    public Button BtnTetris;

    void Start ()
    {
        Cursor.visible = true;

        BtnModele.onClick.AddListener(delegate { ActionLancerScene(1); });
        BtnTestIA.onClick.AddListener(delegate { ActionLancerScene(0); });
        BtnVaisseau.onClick.AddListener(delegate { ActionLancerScene(4); });
        BtnVersion3D.onClick.AddListener(delegate { ActionLancerScene(3); });
        BtnPlateforme.onClick.AddListener(delegate { ActionLancerScene(5); });
        BtnFlappy.onClick.AddListener(delegate { ActionLancerScene(6); });
        BtnTetris.onClick.AddListener(delegate { ActionLancerScene(7); });
    }

    private void ActionLancerScene(int indice)
    {
        SceneManager.LoadScene(indice);
    }

}
