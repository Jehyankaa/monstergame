﻿using UnityEngine;
using System.Collections;

public class JoueurVaisseau : MonoBehaviour
{
    public JeuVaisseau inputManager;
    public float VitesseVaisseau;
    public GameObject Moteur;
    public GameObject Laser;

    private bool isMoving;
    private Rigidbody2D _rigidBody;

    void Start ()
    {
        _rigidBody = GetComponent<Rigidbody2D>();
        Moteur.SetActive(false);
    }
	
	void Update ()
    {
        Vector3 directionOfRotation = inputManager.mousePositionInGameWorld - transform.position;
        transform.rotation = Quaternion.LookRotation(Vector3.forward, directionOfRotation);

        if (isMoving)
        {
            _rigidBody.AddForce(transform.up * VitesseVaisseau);
        }

        float limitXonLeft = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, Camera.main.transform.position.z)).x;
        float limitYBottom = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, Camera.main.transform.position.z)).y;

        float limitXRight = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, Camera.main.transform.position.z)).x;
        float limitYUp = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, Camera.main.transform.position.z)).y;

        Vector3 tempPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z);

        float tempX = Mathf.Clamp(tempPosition.x, limitXonLeft, limitXRight);
        float tempY = Mathf.Clamp(tempPosition.y, limitYBottom, limitYUp);

        tempPosition.x = tempX;
        tempPosition.y = tempY;

        transform.position = tempPosition;

    }

    public void ActivationMoteur()
    {
        isMoving = true;
        Moteur.SetActive(true);
    }

    public void DesactivationMoteur()
    {
        isMoving = false;
        Moteur.SetActive(false);
    }

    public void Tir()
    {
        GameObject newLaser = Instantiate(Laser, transform.position, transform.rotation) as GameObject;
    }
}
