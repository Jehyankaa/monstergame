﻿using UnityEngine;
using System.Collections;

public class LaserVaisseau : MonoBehaviour
{

    public float speed;
    public int Degad;
    private Rigidbody2D rigidBody;
    public GameObject ObjetLaser;

	void Start ()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        rigidBody.AddForce(transform.up*speed, ForceMode2D.Impulse);
	}
	
	void Update ()
    {
	
	}

    void OnCollisionEnter2D(Collision2D collision)
    {
        

        EnnemiVaisseau _ennemi = collision.gameObject.GetComponent<EnnemiVaisseau>();
        JeuVaisseau.Score += _ennemi.PointScore;
        _ennemi.Toucher(Degad);

        Destroy(ObjetLaser);
    }

}
