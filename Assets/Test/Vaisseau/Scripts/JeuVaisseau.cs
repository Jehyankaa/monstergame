﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class JeuVaisseau : MonoBehaviour
{
    public Vector3 mousePositionInGameWorld;
    public JoueurVaisseau player;
    public GameObject target;

    public static int Score = 0;

    public Text txtScore;

    private bool blnPause;
    private bool blnPanneauDemarrage;

    public KeyCode ToucheMoteur= KeyCode.Z;

    public Button BtnRetourMenu;

    void Start ()
    {
        BtnRetourMenu.onClick.AddListener(delegate { ActionMenu(); });

        Cursor.visible = false;
        blnPause = false;
        blnPanneauDemarrage = true;

    }
	
	void Update ()
    {
        if (blnPanneauDemarrage)
        {
            if (!blnPause)
            {
                PositionSouris();
                RecuperTouches();
                RecuperCliqueSouris();
            }

            txtScore.text = "Score : " + Score.ToString();
        }
    }

    private void RecuperCliqueSouris()
    {
        if (Input.GetMouseButtonDown(0))
        {
            player.Tir();
        }
    }

    private void PositionSouris()
    {
        Vector3 mousePositionOnMouse = Input.mousePosition;

        mousePositionInGameWorld = new Vector3(mousePositionOnMouse.x, mousePositionOnMouse.y, Camera.main.transform.position.z * -1);
        mousePositionInGameWorld = Camera.main.ScreenToWorldPoint(mousePositionInGameWorld);

        target.transform.position = mousePositionOnMouse;
    }

    private void RecuperTouches()
    {
        if(Input.GetKey(ToucheMoteur))
        {
            player.ActivationMoteur();
        }

        if (Input.GetKeyUp(ToucheMoteur))
        {
            player.DesactivationMoteur();
        }
    }

    private void ActionMenu()
    {
        SceneManager.LoadScene(2);
    }
}
