﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class CameraFollowPlayer : MonoBehaviour
{
    public GameObject target;
    public Button BtnRetourMenu;

    void Start()
    {
        BtnRetourMenu.onClick.AddListener(delegate { ActionBtnMenu(); });
    }

	void Update ()
    {
        transform.position = new Vector3(target.transform.localPosition.x, transform.position.y, transform.position.z);
	}

    void ActionBtnMenu()
    {
        SceneManager.LoadScene(2);
    }
}
