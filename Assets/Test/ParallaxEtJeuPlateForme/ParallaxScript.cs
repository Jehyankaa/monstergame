﻿using UnityEngine;
using System.Collections;

public class ParallaxScript : MonoBehaviour
{

    float offset;

    public float speed;

    void Update()
    {
        offset += Input.GetAxis("Horizontal") * speed;
        GetComponent<Renderer>().material.SetTextureOffset("_MainTex", new Vector2(offset, 0));
    }
}
