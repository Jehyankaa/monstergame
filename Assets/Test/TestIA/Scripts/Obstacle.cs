﻿using UnityEngine;

public sealed class Obstacle : BaseElement
{

    /// <summary>
    /// Initialisation du Obstacle
    /// </summary>
    void Awake()
    {
        EstJouable = false;
    }


    /// <summary>
    /// Permet de relier le gamestate à l'objet grace au la variable Pos 
    /// </summary>
    public override Position GetStructPos()
    {
        return new Position(transform.position.x, transform.position.y, GetComponent<Renderer>().bounds.min.x , GetComponent<Renderer>().bounds.max.y , GetComponent<Renderer>().bounds.size.x, GetComponent<Renderer>().bounds.size.y, ParametreVelociteX, ParametreVelociteY, ParametreFixe);
    }
}
