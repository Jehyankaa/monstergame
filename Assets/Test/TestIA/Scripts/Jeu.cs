﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(AudioSource))]
public sealed class Jeu : MonoBehaviour
{
    
    public List<GameObject> ListeElements; //obliger d'utiliser une gameobject car unity ne prend pas une interface ou classe abstrate dans l'inspector (c'est un peu de la merde)
    //public Ennemi[] Monstres;


    private IPersonnage[] Personnages;
    private Obstacle[] Obstacles;

    private byte TailleTab;

    public AudioClip SonAmbianceSource; // pour plus tard
    public Button BtnRelancer;
    public Button BtnRetourMenu;

    private EtatJeu EtatJeuEnCours;

    private AudioSource Son;

    public static float Temps;
    public float DeltaTemps;

    private const float C_TempsMax = 0.15f;

    /// <summary>
    /// 
    /// </summary>
    IEnumerator Start()
    {
        //IObservable<string> flux = Observable.Return("Debut"); 


        return InitialisationJeu();

    }

    /// <summary>
    /// Initialisation du jeu
    /// </summary>
    /// <returns></returns>
    private IEnumerator InitialisationJeu()
    {
        Temps = 0f;

        // affectation du bouton a un evenement (provisoire)
        BtnRelancer.onClick.AddListener(delegate { Initialisation(); });
        BtnRetourMenu.onClick.AddListener(delegate { ActionMenu(); });

        InitialisationTableaux();

        //pour le son ambiance (provisoire)
        Son = GetComponent<AudioSource>();
        Son.clip = SonAmbianceSource;
        Son.loop = true;
        Son.priority = 0;
        Son.volume = 80f; // pourra être modif 
        Son.Play();
        yield return new WaitForSeconds(Son.clip.length);  // c'est dans la doc, a voir
    }

    /// <summary>
    /// 
    /// </summary>
    private void InitialisationTableaux()
    {
        byte tour = 0;
        //initialisation de gamestatus
        EtatJeuEnCours = new EtatJeu();

        //foreach(GameObject gobj in ListeElements)

        //var redd = ListeElements.Where(go => go.GetComponent<Ennemi>()).ToArray();

        


        IPersonnage[] tempListe = ListeElements.Componant<Joueur>().ToArray();

        //IPersonnage[] ggg= ListeElements.Componant<Ennemi>().ToArray(); // pour mes tests

        Personnages = tempListe.ConcatTableau(ListeElements.Componant<Ennemi>().ToArray());


        EtatJeuEnCours.Personnages = new EtatPersonnage[Personnages.Length];

        for (tour = 0; tour < Personnages.Length; tour++)
        {
            EtatJeuEnCours.Personnages[tour] = new EtatPersonnage(Personnages[tour].GetStructPos(), Personnages[tour].EstJouable);
        }

        //EtatJeuEnCours.Monstres = new EtatMonstre[Monstres.Length];

        //for (tour = 0; tour < Monstres.Length; tour++)
        //{
        //    EtatJeuEnCours.Monstres[tour] = new EtatMonstre(Monstres[tour].GetStructPos());
        //}

        Obstacles = ListeElements.Componant<Obstacle>().ToArray();
        EtatJeuEnCours.Obstacles = new EtatObstacles[Obstacles.Length];

        for (tour = 0; tour < Obstacles.Length; tour++)
        {
            EtatJeuEnCours.Obstacles[tour] = new EtatObstacles(Obstacles[tour].GetStructPos());
        }


        if (Obstacles.Length > Personnages.Length)
            TailleTab = (byte)Obstacles.Length;
        else
            TailleTab = (byte)Personnages.Length;

    }

    /// <summary>
    /// Boucle de jeu (provisoire)
    /// </summary>
    void Update()
    {
        DeltaTemps = Time.deltaTime;

        if (Temps > C_TempsMax)
            Temps = 0f;

        Deplacement(0);
        DeplacementMonster(1);

        EtatJeuEnCours.Personnages = IA.RechercheSolution(ref EtatJeuEnCours);

        JoueurCollision(ref EtatJeuEnCours.Personnages, ref EtatJeuEnCours.Obstacles);
        //EnnemiCollision(ref EtatJeuEnCours.Monstres, ref EtatJeuEnCours.Obstacles, ref EtatJeuEnCours.Personnages);

        GestionSons();
        Affichages(true);

        Temps += DeltaTemps;


    }

    /// <summary>
    /// 
    /// </summary>
    private void GestionSons()
    {
        byte tour = 0;

        for (tour = 0; tour < EtatJeuEnCours.Personnages.Length; tour++)
        {
            if (EtatJeuEnCours.Personnages[tour].SonPas)
            {
                Personnages[tour].JouerSonPas();
                EtatJeuEnCours.Personnages[tour].SonPas = false;
            }

        }
    }

    ///// <summary>
    ///// 
    ///// </summary>
    ///// <param name="monstres"></param>
    ///// <param name="obstacles"></param>
    ///// <param name="joueurs"></param>
    //public void EnnemiCollision(ref EtatPersonnage[] joueurs, ref EtatObstacles[] obstacles, )
    //{

    //    for (byte unite = 0; unite < monstres.Length; unite++)
    //    {
    //        for (byte tour = 0; tour < obstacles.Length; tour++)
    //        {
    //            if (Collision(ref monstres[unite].Pos, ref obstacles[tour].Pos))
    //                monstres[unite].Pos.BloquerDeplacement(DeltaTemps);
    //        }

    //        for (byte indice = 0; indice < joueurs.Length; indice++)
    //        {
    //            if (Collision(ref monstres[unite].Pos, ref joueurs[indice].Pos))
    //            {
    //                joueurs[indice].Mort();
    //                monstres[unite].Pos.Fixe = true;
    //                print("toucher joueur Mort");
    //            }


    //        }
    //    }

    //}

    /// <summary>
    /// 
    /// </summary>
    /// <param name="joueurs"></param>
    /// <param name="obstacles"></param>
    public void JoueurCollision(ref EtatPersonnage[] joueurs, ref EtatObstacles[] obstacles)
    {
        for (byte indice = 0; indice < joueurs.Length; indice++)
        {
            for (byte p = 0; p < joueurs.Length; p++)
            {
                if (indice != p && !joueurs[p].PersoJouable && !joueurs[p].EstMort)
                {
                    if (Collision(ref joueurs[indice].Pos, ref joueurs[p].Pos))
                    {
                        joueurs[indice].Mort();
                        //joueurs[p].Pos.Fixe = true;
                        print("toucher joueur Mort");
                    }
                }
            }

            for (byte tour = 0; tour < obstacles.Length; tour++)
            {

                if (Collision(ref joueurs[indice].Pos, ref obstacles[tour].Pos))
                {

                    if (obstacles[tour].Pos.Fixe || !joueurs[indice].PersoJouable)
                    {
                        joueurs[indice].Pos.BloquerDeplacement(DeltaTemps);
                    }
                    else
                    {
                        Position positionFutur = obstacles[tour].Pos;

                        positionFutur.Sens = obstacles[tour].Pos.Sens = joueurs[indice].Pos.Sens;
                        positionFutur.DeplacementAuto(DeltaTemps);


                        for (byte t = 0; t < obstacles.Length; t++)
                        {
                            if (t != tour)
                            {
                                if (Collision(ref positionFutur, ref obstacles[t].Pos))
                                {
                                    //bougeCaisse = false;
                                    joueurs[indice].Pos.BloquerDeplacement(DeltaTemps);
                                    obstacles[tour].Pos.BloquerDeplacement(DeltaTemps, false);

                                    break;
                                }
                            }
                        }

                        for (byte m = 0; m < TailleTab; m++)
                        {
                           
                            if (m < Obstacles.Length && m != tour)
                            {
                                if (Collision(ref positionFutur, ref obstacles[m].Pos))
                                {
                                    //bougeCaisse = false;
                                    joueurs[indice].Pos.BloquerDeplacement(DeltaTemps);
                                    obstacles[tour].Pos.BloquerDeplacement(DeltaTemps, false);

                                    break;
                                }
                            }

                            if (m < Personnages.Length)
                            {
                                if (indice != m && !joueurs[m].PersoJouable && !joueurs[m].EstMort)
                                {
                                    if (Collision(ref positionFutur, ref joueurs[m].Pos))
                                    {
                                        joueurs[indice].Pos.BloquerDeplacement(DeltaTemps);
                                        obstacles[tour].Pos.BloquerDeplacement(DeltaTemps, false);

                                        break;
                                    }
                                }
                            }
                        }

                        if (obstacles[tour].Pos.EstBloquer(joueurs[indice].Pos.Sens))
                        {
                            joueurs[indice].Pos.BloquerDeplacement(DeltaTemps);
                            obstacles[tour].Pos.BloquerDeplacement(DeltaTemps, false);
                        }
                        else
                        {
                            obstacles[tour].Pos.Sens = joueurs[indice].Pos.Sens;
                            obstacles[tour].Pos.DeplacementAuto(DeltaTemps);
                        }
                    }
                }
            }

            joueurs[indice].ControleAutorisationDeplacement();
        }

    }

    /// <summary>
    /// Permet de tester si deux rectangles sont rentrés en collision
    /// </summary>
    /// <param name="hitbox_1"></param>
    /// <param name="hitbox_2"></param>
    /// <returns></returns>
    public bool Collision(ref Position hitbox_1, ref Position hitbox_2)
    {
        return !((hitbox_2.OrigineX >= hitbox_1.OrigineX + hitbox_1.Largueur)
        || (hitbox_2.OrigineX + hitbox_2.Largueur <= hitbox_1.OrigineX)
        || (hitbox_2.OrigineY <= hitbox_1.OrigineY - hitbox_1.Hauteur)
        || (hitbox_2.OrigineY - hitbox_2.Hauteur >= hitbox_1.OrigineY));
    }

    /// <summary>
    /// Gestion de l'affichage des objets à l'écran
    /// </summary>
    /// <param name="debug">Permet d'affiché les hitbox (mode debug)</param>
    private void Affichages(bool debug = false)
    {
        byte tour = 0;

        //for (tour = 0; tour < Personnages.Length; tour++)
        //{
        //    Personnages[tour].Affichage(ref EtatJeuEnCours.Personnages[tour].Pos, true);
        //}

        for (tour = 0; tour < TailleTab; tour++)
        {
            if (tour < Personnages.Length)
                Personnages[tour].Affichage(ref EtatJeuEnCours.Personnages[tour].Pos, true);

            if (tour < Obstacles.Length)
                Obstacles[tour].Affichage(ref EtatJeuEnCours.Obstacles[tour].Pos, true);
        }
    }

    /// <summary>
    /// Gestion du déplacement d'un joueur via une manette
    /// </summary>
    /// <param name="indice"></param>
    private void Deplacement(byte indice = 0)
    {
        if (Input.GetAxis("Vertical") > 0)
        {
            EtatJeuEnCours.Personnages[indice].Pos.DeplacementHaut(DeltaTemps);
            EtatJeuEnCours.Personnages[indice].SonPas = true;
        }
        else if (Input.GetAxis("Vertical") < 0)
        {
            EtatJeuEnCours.Personnages[indice].Pos.DeplacementBas(DeltaTemps);
            EtatJeuEnCours.Personnages[indice].SonPas = true;
        }
        else if (Input.GetAxis("Horizontal") < 0)
        {
            EtatJeuEnCours.Personnages[indice].Pos.DeplacementGauche(DeltaTemps);
            EtatJeuEnCours.Personnages[indice].SonPas = true;
        }
        else if (Input.GetAxis("Horizontal") > 0)
        {
            EtatJeuEnCours.Personnages[indice].Pos.DeplacementDroite(DeltaTemps);
            EtatJeuEnCours.Personnages[indice].SonPas = true;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="indice"></param>
    private void DeplacementMonster(byte indice = 0)
    {
        if (Input.GetKeyDown(KeyCode.O))
        {
            EtatJeuEnCours.Personnages[indice].Pos.DeplacementHaut(DeltaTemps);
        }
        else if (Input.GetKeyDown(KeyCode.L))
        {
            EtatJeuEnCours.Personnages[indice].Pos.DeplacementBas(DeltaTemps);
        }
        else if (Input.GetKeyDown(KeyCode.K))
        {
            EtatJeuEnCours.Personnages[indice].Pos.DeplacementGauche(DeltaTemps);
        }
        else if (Input.GetKeyDown(KeyCode.M))
        {
            EtatJeuEnCours.Personnages[indice].Pos.DeplacementDroite(DeltaTemps);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private void Initialisation()
    {
        if (Son != null)
            Son.Stop();

        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }

    private void ActionMenu()
    {
        SceneManager.LoadScene(2);
    }
}
