﻿/// <summary>
/// Interface permettant de stocker dans une seul structure les joueurs et les monstres
/// </summary>
public interface IPersonnage
{
    bool EstJouable {get;}

    Position GetStructPos();
    void DebugHitBox(ref Position pos);
    void Affichage(ref Position pos, bool debug = false);
    void JouerSonPas();
}
