﻿using UnityEngine;

/// <summary>
/// 
/// </summary>
public sealed class Ennemi : BaseElement,IPersonnage
{
    /// <summary>
    /// 
    /// </summary>
    private static Color[] tabCouleur = { Color.red, Color.green, Color.blue, Color.yellow };

    /// <summary>
    /// 
    /// </summary>
    public CouleursEnnemi ParametreCouleur = CouleursEnnemi.ALEATOIRE;

    /// <summary>
    /// Initialisation du monstre
    /// </summary>
    void Awake()
    {
        EstJouable = false;
        SpriteRenderer spriteRenderer;
        spriteRenderer = GetComponent<SpriteRenderer>();

        Color tempCouleur = Color.white;

        if (ParametreCouleur == CouleursEnnemi.ALEATOIRE)
            tempCouleur = tabCouleur[Service.Instance.Rnd.Next(0, 3)];
        else
            tempCouleur = tabCouleur[(int)ParametreCouleur];

        spriteRenderer.material.SetColor("_Color", tempCouleur);
    }

    /// <summary>
    /// Permet de relier le gamestate à l'objet grace au la variable Pos 
    /// </summary>
    public override Position GetStructPos()
    {
        return new Position(transform.position.x, transform.position.y, GetComponent<Renderer>().bounds.min.x, GetComponent<Renderer>().bounds.max.y,GetComponent<Renderer>().bounds.size.x, GetComponent<Renderer>().bounds.size.y, ParametreVelociteX, ParametreVelociteY);
    }

    /// <summary>
    /// 
    /// </summary>
    public void JouerSonPas()
    {

    }


}
