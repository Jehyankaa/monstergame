﻿public static class IA
{
    
    public static byte? RechercheCible(ref EtatPersonnage[] joueurs)
    {
        byte? indice = null;

        for (byte tour=0;tour < joueurs.Length;tour++)
        {
            if (!joueurs[tour].EstMort)
            {
                indice = tour;
                break;
            }
        }

        return indice;
    }

    public static EtatPersonnage[] RechercheSolution(ref EtatJeu etatJeu)
    {

        //etatJeu.Monstres[0].Pos.DeplacementBas();

        return etatJeu.Personnages;
    }

    private static EtatJeu[] Transition(EtatJeu etatJeu)
    {


        return null;
    }

    private static float Heuristique()
    {
        return 0;
    }

    private static float Distance(ref float x,ref float y, ref float xCible,ref float yCible)
    {
        return (float)System.Math.Sqrt(System.Math.Pow((xCible - x), 2) + System.Math.Pow((yCible - y), 2));
    }
	
}
