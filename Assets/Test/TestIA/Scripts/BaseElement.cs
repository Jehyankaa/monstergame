﻿using UnityEngine;

public abstract class BaseElement : MonoBehaviour
{
    /// <summary>
    /// Permet de définir la vitesse de déplacement sur l'axe des X
    /// </summary>
    public float ParametreVelociteX;

    /// <summary>
    /// Permet de définir la vitesse de déplacement sur l'axe des Y
    /// </summary>
    public float ParametreVelociteY;

    /// <summary>
    /// Permet de le définir comme un élément fixe comme par exemple un mur
    /// </summary>
    public bool ParametreFixe;

    /// <summary>
    /// Variable temporelle pour convertion les coordonnées Position en Vector
    /// </summary>
    protected Vector2 TempCoordonnees = Vector2.zero;

    /// <summary>
    /// Permet de définir comme un élément jouable comme un joueur
    /// </summary>
    public bool EstJouable { get; protected set; }

    /// <summary>
    /// Permet de créer et de récuper une structure 
    /// </summary>
    /// <returns></returns>
    public abstract Position GetStructPos();

    /// <summary>
    /// Affiche la hitBox 
    /// </summary>
    /// <remarks> Mode Debug</remarks>
    public void DebugHitBox(ref Position pos)
    {
        Debug.DrawLine(pos.GetA(), pos.GetB(), Color.red);
        Debug.DrawLine(pos.GetA(), pos.GetC(), Color.red);
        Debug.DrawLine(pos.GetB(), pos.GetD(), Color.red);
        Debug.DrawLine(pos.GetC(), pos.GetD(), Color.red);
    }

    /// <summary>
    /// Gestion de l'affichage des objets à l'écran
    /// </summary>
    /// <param name="pos">Coordonnées</param>
    /// <param name="debug">Permet d'affiché les hitbox (mode debug)</param>
    public virtual void Affichage(ref Position pos,bool debug = false)
    {
        TempCoordonnees.x = pos.X;
        TempCoordonnees.y = pos.Y;

        transform.position = TempCoordonnees;

        if (debug)
            DebugHitBox(ref pos);

    }

}
