﻿
#region Déclaration des structures


[System.Runtime.CompilerServices.UnsafeValueType]
public struct EtatPersonnage
{
    public Position Pos;
    public bool EstMort;
    public bool PersoJouable;
    public bool SonPas;


    public EtatPersonnage(Position pos,bool jouable=false)
    {
        Pos = pos;

        EstMort = false;
        SonPas = false;
        PersoJouable=jouable;
    }

    public void Mort()
    {
        EstMort = true;

        Pos.BloquerDroite = true;
        Pos.BloquerGauche = true;
        Pos.BloquerHaute = true;
        Pos.BloquerBas = true;        
    }

    public void ControleAutorisationDeplacement()
    {
        if (SonPas && Pos.Sens == Direction.STOP)
        {
            SonPas = false;
        }
    }

};

//[System.Runtime.CompilerServices.UnsafeValueType]
//public struct EtatMonstre
//{
//    public Position Pos;

//    public bool Bloquer;



//    public EtatMonstre(Position pos)
//    {
//        Pos = pos;

//        Bloquer = false;


//    }
//};

[System.Runtime.CompilerServices.UnsafeValueType]
public struct EtatObstacles
{
    public Position Pos;

    public EtatObstacles(Position pos)
    {
        Pos = pos;
    }
};

[System.Runtime.CompilerServices.UnsafeValueType]
public struct EtatJeu
{
    public EtatPersonnage[] Personnages;
    //public EtatMonstre[] Monstres;
    public EtatObstacles[] Obstacles;

    public bool ModifEtatObstacles;
};

[System.Runtime.CompilerServices.UnsafeValueType]
public struct Position
{
    public float X;
    public float Y;

    public float OrigineX;
    public float OrigineY;
    public float Largueur;
    public float Hauteur;

    public float VelociteX;
    public float VelociteY;

    public Direction Sens;

    public bool Fixe;

    public bool BloquerDroite;
    public bool BloquerGauche;
    public bool BloquerHaute;
    public bool BloquerBas;

    public Position(float x, float y, float origineX,float origineY, float largueur, float hauteur, float velociteX, float velociteY,bool fixe=false)
    {
        X = x;
        Y = y;


        OrigineX = origineX;
        OrigineY = origineY;
        Largueur = largueur;
        Hauteur = hauteur;

        VelociteX = velociteX;
        VelociteY = velociteY;

        Sens = Direction.STOP;

        Fixe = fixe;

        BloquerDroite = false;
        BloquerGauche = false;
        BloquerHaute = false;
        BloquerBas = false;
    }

    public void DeplacementHaut(float facteur)
    {

        if (!(BloquerHaute || Fixe))
        {
            Y += VelociteY * facteur;
            OrigineY += VelociteY * facteur;
            Sens = Direction.HAUT;

            RazMemoireDeplacement();
        }
    }

    public void DeplacementBas(float facteur)
    {
        if (!(BloquerBas || Fixe))
        { 
            Y -= VelociteY * facteur;
            OrigineY -= VelociteY * facteur;
            Sens = Direction.BAS;

            RazMemoireDeplacement();
        }
    }

    public void DeplacementGauche(float facteur)
    {
        if (!(BloquerGauche || Fixe))
        {
            X -= VelociteX * facteur;
            OrigineX -= VelociteX * facteur;
            Sens = Direction.GAUCHE;

            RazMemoireDeplacement();
        }
    }

    public void DeplacementDroite(float facteur)
    {
        if (!(BloquerDroite || Fixe))
        {
            X += VelociteX * facteur;
            OrigineX  += VelociteX * facteur;
            Sens = Direction.DROITE;

            RazMemoireDeplacement();
        }
    }

    public bool EstBloquer(Direction sensjoueur)
    {
        bool retour = false;

        switch (sensjoueur)
        {
            case Direction.DROITE:
                retour = BloquerDroite;
                break;
            case Direction.GAUCHE:
                retour = BloquerGauche;
                break;
            case Direction.HAUT:
                retour = BloquerHaute;
                break;
            case Direction.BAS:
                retour = BloquerBas;
                break;

        }

        return retour;
    }

    public void DeplacementAuto(float facteur)
    {
        switch(Sens)
        {
            case Direction.DROITE:
                DeplacementDroite(facteur);
                break;
            case Direction.GAUCHE:
                DeplacementGauche(facteur);
                break;
            case Direction.HAUT:
                DeplacementHaut(facteur);
                break;
            case Direction.BAS:
                DeplacementBas(facteur);
                break;

        }
    }

    public void RazMemoireDeplacement()
    {
        BloquerDroite = false;
        BloquerGauche = false;
        BloquerHaute = false;
        BloquerBas = false;
    }

    public void BloquerDeplacement(float facteur,bool annulationDeplacement=true)
    {
        if (Sens == Direction.HAUT)
        {
            BloquerHaute = true;

            if (annulationDeplacement)
            {
                Y -= VelociteY * facteur;
                OrigineY -= VelociteY * facteur;
            }

            Sens = Direction.STOP;
        }
        else if (Sens == Direction.BAS)
        {
            BloquerBas = true;

            if (annulationDeplacement)
            {
                Y += VelociteY * facteur;
                OrigineY += VelociteY * facteur;
            }

            Sens = Direction.STOP;
        }
        else if (Sens == Direction.DROITE)
        {
            BloquerDroite = true;

            if (annulationDeplacement)
            {
                X -= VelociteX * facteur;
                OrigineX -= VelociteX * facteur;
            }

            Sens = Direction.STOP;
        }
        else if (Sens == Direction.GAUCHE)
        {
            BloquerGauche = true;

            if (annulationDeplacement)
            {
                X += VelociteX * facteur;
                OrigineX += VelociteX * facteur;
            }

            Sens = Direction.STOP;
        }

    }

    public UnityEngine.Vector2 GetA()
    {
        return new UnityEngine.Vector2(OrigineX, OrigineY);
    }

    public UnityEngine.Vector2 GetB()
    {
        return new UnityEngine.Vector2(OrigineX + Largueur, OrigineY);
    }

    public UnityEngine.Vector2 GetC()
    {
        return new UnityEngine.Vector2(OrigineX, OrigineY - Hauteur);
    }

    public UnityEngine.Vector2 GetD()
    {
        return new UnityEngine.Vector2(OrigineX + Largueur, OrigineY - Hauteur);
    }

};

[System.Runtime.CompilerServices.UnsafeValueType]
public struct Noeud
{
    public EtatJeu EtatJeuNoeud;
    public float? Cout;
    public uint Cible;

    public Noeud(EtatJeu etatJeu,uint cible)
    {
        EtatJeuNoeud = etatJeu;
        Cible = cible;
        Cout = null;
    }

    public bool ControleBut()
    {
        return EtatJeuNoeud.Personnages[Cible].EstMort;
    }

}

#endregion

#region Déclaration des énumérations


public enum CouleursEnnemi
{
    ALEATOIRE,
    ROUGE = 0,
    VERT = 1,
    BLEU = 2,
    JAUNE = 3
}

public enum Direction
{
    STOP,DROITE,GAUCHE,HAUT,BAS
}

/// <summary>
/// Enumération numéro du joueur
/// </summary>
public enum JoueurNum
{
    JOUEUR1, JOUEUR2
}

#endregion


