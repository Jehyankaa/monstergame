﻿using UnityEngine;
using System.Collections;

public sealed class Joueur : BaseElement,IPersonnage
{
    /// <summary>
    /// 
    /// </summary>
    public Sprite[] ImagesJoueur1;

    /// <summary>
    /// 
    /// </summary>
    public Sprite[] ImagesJoueur2;

    /// <summary>
    /// 
    /// </summary>
    public JoueurNum ChoixNumJoueur=JoueurNum.JOUEUR1;

    /// <summary>
    /// 
    /// </summary>
    public AudioClip BruitPas; 
    
    /// <summary>
    /// 
    /// </summary>
    private AudioSource Son;

    /// <summary>
    /// 
    /// </summary>
    private byte CompteurSprite;

    /// <summary>
    /// 
    /// </summary>
    private SpriteRenderer SpriteRenderer;

    /// <summary>
    /// 
    /// </summary>
    public static float C_TempsMaxAnimationPerso = 0.15f;

    /// <summary>
    /// Structure 
    /// </summary>
    public Position Pos;

    /// <summary>
    /// Initialisation du joueur
    /// </summary>
    void Awake()
    {
        EstJouable = true;
        SpriteRenderer = GetComponent<SpriteRenderer>();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    IEnumerator Start()
    {
        Son = GetComponent<AudioSource>();

        Son.clip = BruitPas;
        Son.volume = 90f; // pourra être modif 
        yield return new WaitForSeconds(Son.clip.length);  // c'est dans la doc, a voir

        CompteurSprite = 0;
    }
    
    /// <summary>
    /// Permet de relier le gamestate à l'objet grace au la variable Pos 
    /// </summary>
    public override Position GetStructPos()
    {
        return new Position(transform.position.x, transform.position.y, GetComponent<Renderer>().bounds.min.x, GetComponent<Renderer>().bounds.max.y, GetComponent<Renderer>().bounds.size.x, GetComponent<Renderer>().bounds.size.y, ParametreVelociteX, ParametreVelociteY);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="pos"></param>
    /// <param name="debug"></param>
    public override void Affichage(ref Position pos, bool debug = false)
    {
        if (Jeu.Temps >= C_TempsMaxAnimationPerso || Jeu.Temps == 0)
            AnimationSprite(ref pos.Sens);

        pos.Sens = Direction.STOP; // stop animation

        base.Affichage(ref pos, debug);
    }

    /// <summary>
    /// 
    /// </summary>
    public void JouerSonPas()
    {
        if (!Son.isPlaying)
        {
            Son.Play();
        }

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sens"></param>
    private void AnimationSprite(ref Direction sens)
    {
        
        switch(sens)
        {
            case Direction.BAS:

                if (CompteurSprite < 1 || CompteurSprite > 2)
                    CompteurSprite = 1;

                break;

            case Direction.HAUT:

                if (CompteurSprite < 7 || CompteurSprite > 9)
                    CompteurSprite = 7;

                break;

            case Direction.DROITE:

                if (CompteurSprite < 3 || CompteurSprite > 4)
                    CompteurSprite = 3;

                break;

            case Direction.GAUCHE:

                if (CompteurSprite < 5 || CompteurSprite > 6)
                    CompteurSprite =5;
                    
                break;

            default:

                CompteurSprite = 0;

                break;
        }

        if (ChoixNumJoueur == JoueurNum.JOUEUR1)
            SpriteRenderer.sprite = ImagesJoueur1[CompteurSprite];
        else if (ChoixNumJoueur == JoueurNum.JOUEUR2)
            SpriteRenderer.sprite = ImagesJoueur2[CompteurSprite];

        CompteurSprite++;
    }

}
