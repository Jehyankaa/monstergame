﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public static class Outils
{
    /// <summary>
    /// Méthode d'extention de IEnumerable
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="liste"></param>
    /// <returns>Retourne une IEnumerable de même type que le parametre T de la méthode générique </returns>
    public static IEnumerable<T> Componant<T>(this IEnumerable<GameObject> liste)
    where T : UnityEngine.Component
    {
        foreach (GameObject item in liste)
        {
       
                var componant = item.GetComponent<T>();
                if (componant != null)
                {
                    yield return componant;
                }

        }
    }

    /// <summary>
    /// Méthode générique qui permet de concatener deux tableaux
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="tableauA"></param>
    /// <param name="tableauB"></param>
    /// <returns></returns>
    public static T[] ConcatTableau<T>(this T[] tableauA, T[] tableauB) 
    {
        // merci internet  (http://codes-sources.commentcamarche.net/forum/affich-619784-concatener-2-tableaux)
        T[] retour = new T[tableauA.Length + tableauB.Length];

        tableauA.CopyTo(retour, 0);
        Array.ConstrainedCopy(tableauB, 0, retour, tableauA.Length, tableauB.Length);

        return retour;
    }
}
