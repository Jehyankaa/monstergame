﻿using UnityEngine;
using System.Collections;
using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Version3D
{
    public enum TypeElement { Vide, Joueur, Ennemi, Caisse, Mur };
    public enum Direction { AUCUNE, GAUCHE, DROITE, HAUT, BAS };
    public enum VueCamera { DESSUS, TPS };

    /// <summary>
    /// 
    /// </summary>
    public struct PointGrille
    {
        public int? IndiceTab;// Indice des tableaux gameobject


        public bool Visiter;
        public bool Modification; // pour l'affichage

        public TypeElement Type;
        public Coordonnee? Parent;

        public PointGrille(TypeElement type, int? indice)
        {
            Type = type;
            Visiter = false;

            Modification = false;
            IndiceTab = indice;
            Parent = null;

        }
    }

    public struct Noeud
    {
        public Coordonnee Position;
        public int? NoeudParent;
    }

    /// <summary>
    /// 
    /// </summary>
    public struct CaisseJoueurDeplacement
    {
        public Coordonnee Ancienne;
        public Coordonnee Nouvelle;
    }


    /// <summary>
    /// 
    /// </summary>
    public struct Coordonnee
    {
        public int X;
        public int Y;

        public Coordonnee(int y, int x)
        {
            Y = y;
            X = x;
        }
    }


    /// <summary>
    /// 
    /// </summary>
    public struct ReponseControle
    {
        public bool DeplacementPossible;
        public CaisseJoueurDeplacement? CaisseDeplacer;
    }




    /// <summary>
    /// 
    /// </summary>
    public class Jeu : MonoBehaviour
    {
        public PointGrille[,] Grille;

        public GameObject CameraTPS;
        public GameObject MiniCarte;
        private VueCamera TypeVue = VueCamera.DESSUS;

        public GameObject[] ElementsJoueur;
        public GameObject[] ElementsEnnemi;
        public GameObject[] ElementsMur;
        public GameObject[] ElementsCaisse;

        private Noeud[] Noeuds;
        private List<Coordonnee> CheminsPourEnnemi = new List<Coordonnee>();

        public float DeltaTemps = 0f;

        private float Temps = 0f;

        private int IndiceMaxGrilleX = 0;
        private int IndiceMaxGrilleY = 0;
        private int IndiceMaxNoeuds = 0;


        private const float C_TempsMax = 0.4f;
        private const int C_MaxCapaciteGrille = 50;
        private const int C_MaxCapaciteNoeud = 2500;

        private Vector3 TempVector = Vector3.zero;


        public Button BtnRetourMenu;

        /// <summary>
        /// 
        /// </summary>
        void Start()
        {
            BtnRetourMenu.onClick.AddListener(delegate { ActionMenu(); });

            InitialisationGrille();
        }

        /// <summary>
        /// 
        /// </summary>
        void Update()
        {
            Coordonnee joueur = RecherchePremier(TypeElement.Joueur);

            if (!(joueur.X == -1 && joueur.Y == -1))
            {

                DeltaTemps = Time.deltaTime;

                Deplacement();


                if (Temps > C_TempsMax)
                {
                    Temps = 0f;

                    RAZVisiter();
                    IA();

                    DeplacementEnnemi();
                }


                if (Input.GetKeyDown(KeyCode.C))
                {
                    if (TypeVue == VueCamera.DESSUS)
                        TypeVue = VueCamera.TPS;
                    else
                        TypeVue = VueCamera.DESSUS;

                    ChangeCamera(TypeVue);
                }

                Affichage();

                Temps += DeltaTemps;
            }
            else
            {
                ElementsJoueur[0].SetActive(false);
            }
        }


        private void DeplacementEnnemi()
        {
            Coordonnee ennemi = RecherchePremier(TypeElement.Ennemi);
            //Coordonnee joueur = RecherchePremier(TypeElement.Joueur);

            //if (joueur.X != ennemi.X && joueur.Y != ennemi.Y)
            //{
            Grille[CheminsPourEnnemi.First().Y, CheminsPourEnnemi.First().X] = Grille[ennemi.Y, ennemi.X];
            Grille[CheminsPourEnnemi.First().Y, CheminsPourEnnemi.First().X].Modification = true;

            Grille[ennemi.Y, ennemi.X] = new PointGrille(TypeElement.Vide, null);

            CheminsPourEnnemi.RemoveAt(0);
            //}
        }

        private void IA()
        {
            bool parcours = true;
            bool joueurTrouve = false;
            //bool cibleTrouver = false;

            Noeud curseur = new Noeud();
            Noeud? futur;
            int indice;

            List<int> indiceNoeuds = new List<int>();
            IndiceMaxNoeuds = 0;

            Noeuds = new Noeud[C_MaxCapaciteNoeud];


            Noeuds[IndiceMaxNoeuds] = new Noeud() { Position = RecherchePremier(TypeElement.Ennemi), NoeudParent = null };
            indiceNoeuds.Add(0);


            do
            {

                if (indiceNoeuds.Count > 0)
                {
                    indice = indiceNoeuds.First();

                    curseur = Noeuds[indice];
                    indiceNoeuds.RemoveAt(0);

                    if (ControleNoeud(curseur.Position.Y + 1, curseur.Position.X, out futur, indice, out joueurTrouve) && parcours)
                    {
                        IndiceMaxNoeuds++;

                        indiceNoeuds.Add(IndiceMaxNoeuds);

                        Noeuds[IndiceMaxNoeuds] = futur.Value;

                        parcours = !joueurTrouve;

                    }

                    if (ControleNoeud(curseur.Position.Y, curseur.Position.X + 1, out futur, indice, out joueurTrouve) && parcours)
                    {
                        IndiceMaxNoeuds++;

                        indiceNoeuds.Add(IndiceMaxNoeuds);

                        Noeuds[IndiceMaxNoeuds] = futur.Value;

                        parcours = !joueurTrouve;

                    }


                    if (ControleNoeud(curseur.Position.Y - 1, curseur.Position.X, out futur, indice, out joueurTrouve) && parcours)
                    {
                        IndiceMaxNoeuds++;

                        indiceNoeuds.Add(IndiceMaxNoeuds);

                        Noeuds[IndiceMaxNoeuds] = futur.Value;

                        parcours = !joueurTrouve;

                    }

                    if (ControleNoeud(curseur.Position.Y, curseur.Position.X - 1, out futur, indice, out joueurTrouve) && parcours)
                    {
                        IndiceMaxNoeuds++;

                        indiceNoeuds.Add(IndiceMaxNoeuds);

                        Noeuds[IndiceMaxNoeuds] = futur.Value;

                        parcours = !joueurTrouve;
                    }
                }
                else
                {
                    parcours = false;
                }


            } while (parcours);



            // refaire le chemin

            //CheminsPourEnnemi 

            CheminsPourEnnemi.Clear();
            curseur = Noeuds[IndiceMaxNoeuds];

            do
            {
                CheminsPourEnnemi.Add(curseur.Position);

                if (curseur.NoeudParent != null)
                    curseur = Noeuds[curseur.NoeudParent.Value];


            } while (curseur.NoeudParent != null);


            CheminsPourEnnemi.Reverse();

        }


        /// <summary>
        /// 
        /// </summary>
        private void Affichage()
        {
            for (int tempY = 0; tempY < C_MaxCapaciteGrille; tempY++)
            {
                for (int tempX = 0; tempX < C_MaxCapaciteGrille; tempX++)
                {
                    if (Grille[tempY, tempX].Modification)
                    {
                        TempVector.Set(tempX, 0, tempY);

                        if (Grille[tempY, tempX].Type == TypeElement.Joueur)
                        {
                            ElementsJoueur[Grille[tempY, tempX].IndiceTab.Value].transform.position = TempVector;
                            ElementsJoueur[Grille[tempY, tempX].IndiceTab.Value].SetActive(true);
                        }
                        else if (Grille[tempY, tempX].Type == TypeElement.Ennemi)
                        {
                            ElementsEnnemi[Grille[tempY, tempX].IndiceTab.Value].transform.position = TempVector;
                            ElementsEnnemi[Grille[tempY, tempX].IndiceTab.Value].SetActive(true);
                        }
                        else if (Grille[tempY, tempX].Type == TypeElement.Caisse)
                        {
                            ElementsCaisse[Grille[tempY, tempX].IndiceTab.Value].transform.position = TempVector;
                            ElementsCaisse[Grille[tempY, tempX].IndiceTab.Value].SetActive(true);
                        }
                        else if (Grille[tempY, tempX].Type == TypeElement.Mur)
                        {
                            ElementsMur[Grille[tempY, tempX].IndiceTab.Value].transform.position = TempVector;
                            ElementsMur[Grille[tempY, tempX].IndiceTab.Value].SetActive(true);
                        }

                        Grille[tempY, tempX].Modification = false;

                    }
                }
            }


        }

        private void ChangeCamera(VueCamera vue)
        {
            if (vue == VueCamera.TPS)
            {
                CameraTPS.SetActive(true);
                MiniCarte.SetActive(true);
            }
            else
            {
                CameraTPS.SetActive(false);
                MiniCarte.SetActive(false);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void InitialisationGrille()
        {
            Grille = new PointGrille[C_MaxCapaciteGrille, C_MaxCapaciteGrille];

            for (int tempY = 0; tempY < C_MaxCapaciteGrille; tempY++)
            {
                for (int tempX = 0; tempX < C_MaxCapaciteGrille; tempX++)
                {
                    Grille[tempY, tempX] = new PointGrille(TypeElement.Vide, null);
                }
            }

            int indice = 0;

            foreach (GameObject objJoueur in ElementsJoueur)
            {
                Grille[(int)objJoueur.transform.position.z, (int)objJoueur.transform.position.x].Type = TypeElement.Joueur;
                Grille[(int)objJoueur.transform.position.z, (int)objJoueur.transform.position.x].IndiceTab = indice;
                Grille[(int)objJoueur.transform.position.z, (int)objJoueur.transform.position.x].Modification = true;

                if (objJoueur.transform.position.x > IndiceMaxGrilleX)
                    IndiceMaxGrilleX = (int)objJoueur.transform.position.x;

                if (objJoueur.transform.position.z > IndiceMaxGrilleY)
                    IndiceMaxGrilleY = (int)objJoueur.transform.position.z;

                indice++;
            }

            indice = 0;

            foreach (GameObject objEnnemi in ElementsEnnemi)
            {
                Grille[(int)objEnnemi.transform.position.z, (int)objEnnemi.transform.position.x].Type = TypeElement.Ennemi;
                Grille[(int)objEnnemi.transform.position.z, (int)objEnnemi.transform.position.x].IndiceTab = indice;
                Grille[(int)objEnnemi.transform.position.z, (int)objEnnemi.transform.position.x].Modification = true;

                if (objEnnemi.transform.position.x > IndiceMaxGrilleX)
                    IndiceMaxGrilleX = (int)objEnnemi.transform.position.x;

                if (objEnnemi.transform.position.z > IndiceMaxGrilleY)
                    IndiceMaxGrilleY = (int)objEnnemi.transform.position.z;

                indice++;
            }

            indice = 0;

            foreach (GameObject objMur in ElementsMur)
            {

                Grille[(int)objMur.transform.position.z, (int)objMur.transform.position.x].Type = TypeElement.Mur;
                Grille[(int)objMur.transform.position.z, (int)objMur.transform.position.x].IndiceTab = indice;
                Grille[(int)objMur.transform.position.z, (int)objMur.transform.position.x].Modification = true;

                if (objMur.transform.position.x > IndiceMaxGrilleX)
                    IndiceMaxGrilleX = (int)objMur.transform.position.x;

                if (objMur.transform.position.z > IndiceMaxGrilleY)
                    IndiceMaxGrilleY = (int)objMur.transform.position.z;

                indice++;
            }

            indice = 0;

            foreach (GameObject objCaisse in ElementsCaisse)
            {
                Grille[(int)objCaisse.transform.position.z, (int)objCaisse.transform.position.x].Type = TypeElement.Caisse;
                Grille[(int)objCaisse.transform.position.z, (int)objCaisse.transform.position.x].IndiceTab = indice;
                Grille[(int)objCaisse.transform.position.z, (int)objCaisse.transform.position.x].Modification = true;


                if (objCaisse.transform.position.x > IndiceMaxGrilleX)
                    IndiceMaxGrilleX = (int)objCaisse.transform.position.x;

                if (objCaisse.transform.position.z > IndiceMaxGrilleY)
                    IndiceMaxGrilleY = (int)objCaisse.transform.position.z;

                indice++;
            }

            print("chargement ok");

        }

        /// <summary>
        /// Gestion du déplacement d'un joueur via une manette
        /// </summary>
        /// <param name="indice"></param>
        private void Deplacement(byte indice = 0)
        {
            bool movement = false;

            Coordonnee coord = RecherchePremier(TypeElement.Joueur);

            int tempX = coord.X;
            int tempY = coord.Y;
            Direction direction = Direction.AUCUNE;


            //if (Input.GetAxis("Vertical") > 0)
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                tempY += 1;

                if (tempY <= IndiceMaxGrilleY && tempY >= 0)
                {
                    movement = true;
                    direction = Direction.HAUT;
                }
            }
            else if (Input.GetKeyDown(KeyCode.DownArrow))
            //else if (Input.GetAxis("Vertical") < 0)
            {
                tempY -= 1;

                if (tempY <= IndiceMaxGrilleY && tempY >= 0)
                {
                    movement = true;
                    direction = Direction.BAS;
                }
            }
            else if (Input.GetKeyDown(KeyCode.LeftArrow))
            //else if (Input.GetAxis("Horizontal") < 0)
            {
                tempX -= 1;

                if (tempX <= IndiceMaxGrilleX && tempX >= 0)
                {
                    movement = true;
                    direction = Direction.GAUCHE;
                }
            }
            else if (Input.GetKeyDown(KeyCode.RightArrow))
            //else if (Input.GetAxis("Horizontal") > 0)
            {
                tempX += 1;

                if (tempX <= IndiceMaxGrilleX && tempX >= 0)
                {
                    movement = true;
                    direction = Direction.DROITE;
                }
            }

            if (movement)
            {
                ReponseControle reponse = ControleDeplacement(tempY, tempX, direction);

                if (reponse.DeplacementPossible)
                {

                    if (reponse.CaisseDeplacer != null)
                    {
                        Grille[reponse.CaisseDeplacer.Value.Nouvelle.Y, reponse.CaisseDeplacer.Value.Nouvelle.X] = Grille[reponse.CaisseDeplacer.Value.Ancienne.Y, reponse.CaisseDeplacer.Value.Ancienne.X];
                        Grille[reponse.CaisseDeplacer.Value.Nouvelle.Y, reponse.CaisseDeplacer.Value.Nouvelle.X].Modification = true;

                        Grille[reponse.CaisseDeplacer.Value.Ancienne.Y, reponse.CaisseDeplacer.Value.Ancienne.X] = new PointGrille(TypeElement.Vide, null);
                    }

                    Grille[tempY, tempX] = Grille[coord.Y, coord.X];
                    Grille[tempY, tempX].Modification = true;

                    Grille[coord.Y, coord.X] = new PointGrille(TypeElement.Vide, null);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="y"></param>
        /// <param name="x"></param>
        /// <param name="direction"></param>
        /// <returns></returns>
        private ReponseControle ControleDeplacement(int y, int x, Direction direction = Direction.AUCUNE)
        {
            ReponseControle retour = new ReponseControle();
            retour.DeplacementPossible = false;

            if (Grille[y, x].Type == TypeElement.Mur)
            {
                retour.DeplacementPossible = false;
            }
            else if (Grille[y, x].Type == TypeElement.Caisse)
            {
                int tempY = y;
                int tempX = x;

                if (direction == Direction.HAUT)
                {
                    tempY += 1;
                }
                else if (direction == Direction.BAS)
                {
                    tempY -= 1;
                }
                else if (direction == Direction.DROITE)
                {
                    tempX += 1;
                }
                else if (direction == Direction.GAUCHE)
                {
                    tempX -= 1;
                }

                if (Grille[tempY, tempX].Type == TypeElement.Vide)
                {
                    retour.DeplacementPossible = true;
                    retour.CaisseDeplacer = new CaisseJoueurDeplacement() { Ancienne = new Coordonnee(y, x), Nouvelle = new Coordonnee(tempY, tempX) };
                }
            }
            else
            {
                retour.DeplacementPossible = true;
            }

            return retour;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private Coordonnee RecherchePremier(TypeElement type)
        {
            for (int tempY = 0; tempY < C_MaxCapaciteGrille; tempY++)
            {
                for (int tempX = 0; tempX < C_MaxCapaciteGrille; tempX++)
                {
                    if (Grille[tempY, tempX].Type == type)
                    {
                        return new Coordonnee(tempY, tempX);
                    }
                }
            }

            return new Coordonnee(-1, -1);
        }

        private void RAZVisiter()
        {
            for (int tempY = 0; tempY < C_MaxCapaciteGrille; tempY++)
            {
                for (int tempX = 0; tempX < C_MaxCapaciteGrille; tempX++)
                {
                    Grille[tempY, tempX].Visiter = false;
                    Grille[tempY, tempX].Parent = null;
                }
            }
        }

        private bool ContinuerParcoursTableau()
        {
            for (int tempY = 0; tempY < C_MaxCapaciteGrille; tempY++)
            {
                for (int tempX = 0; tempX < C_MaxCapaciteGrille; tempX++)
                {
                    if (!Grille[tempY, tempX].Visiter)
                        return true;
                }
            }

            return false;
        }

        private bool ControleNoeud(int y, int x, out Noeud? noeudRetour, int indiceNoeudParent, out bool joueurTrouve)
        {
            noeudRetour = null;
            joueurTrouve = false;

            if (x > IndiceMaxGrilleX && x < 0 && y > IndiceMaxGrilleY && y < 0)
                return false;

            if (Grille[y, x].Visiter)
                return false;

            Grille[y, x].Visiter = true;

            if (Grille[y, x].Type != TypeElement.Vide && Grille[y, x].Type != TypeElement.Joueur)
                return false;

            noeudRetour = new Noeud() { Position = new Coordonnee(y, x), NoeudParent = indiceNoeudParent };

            if (Grille[y, x].Type == TypeElement.Joueur)
                joueurTrouve = true;

            return true;
        }

        private void ActionMenu()
        {
            SceneManager.LoadScene(2);
        }
    }
}
