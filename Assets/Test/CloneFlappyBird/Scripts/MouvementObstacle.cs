﻿using UnityEngine;
using System.Collections;

public class MouvementObstacle : MonoBehaviour {

    public Vector2 vitesse = new Vector2(-5, 0);

	// Use this for initialization
	void Start ()
    {
        Rigidbody2D _obj = this.GetComponent<Rigidbody2D>();
        _obj.velocity = vitesse;
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
