﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System;

public class CochonGestion : MonoBehaviour
{
    public Rigidbody2D _rigidBody;

    private Vector2 _vecForce = new Vector2(0, 200);
    Vector2 positionEcran;

    private bool blnToucher;
    private bool blnPartieEnCours;

    private float fTemps;
    private const float C_TempsMax = 3f;

    // Use this for initialization
    void Start ()
    {
        blnToucher = false;
        blnPartieEnCours = false;
        fTemps = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        positionEcran = Camera.main.WorldToScreenPoint(this.transform.position);


        if (blnPartieEnCours)
        {
            if (Input.GetKeyDown(KeyCode.Space) && !blnToucher)
            {
                Rigidbody2D _obj = this.GetComponent<Rigidbody2D>();

                _obj.AddForce(_vecForce);
            }

            if (positionEcran.y > Screen.height || positionEcran.y < 0)
            {
                TuerCochon();
            }
        }
        else
        {
            _rigidBody.isKinematic = true;
        }

        if (fTemps > C_TempsMax && !blnPartieEnCours)
        {
            blnPartieEnCours = true;
            _rigidBody.isKinematic = false;
        }

        fTemps += Time.deltaTime;
    }

    void OnCollisionEntrer2D(Collider2D other)
    {
        Debug.Log("Collision");
        TuerCochon();
    }

    private void TuerCochon()
    {
        SceneManager.LoadScene(6);
    }
}
