﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GestionJeu : MonoBehaviour
{
    public Button BtnRetourMenu;

    // Use this for initialization
    void Start ()
    {
        BtnRetourMenu.onClick.AddListener(delegate { ActionMenu(); });
    }

    private void ActionMenu()
    {
        SceneManager.LoadScene(2);
    }
}
