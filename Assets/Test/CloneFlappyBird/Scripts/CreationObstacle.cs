﻿using UnityEngine;
using System.Collections;

public class CreationObstacle : MonoBehaviour
{
    public GameObject obstacle;

	// Use this for initialization
	void Start ()
    {
        InvokeRepeating("creerObstacle", 1.5f, 1.5f);

    }

    private void creerObstacle()
    {
        Instantiate(obstacle);
    }

}
