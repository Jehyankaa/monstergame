﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Player : Element {
	/*public float PositionX = 1f;
	public float PositionY = 1f;
*/
	public Dictionary<Sens,bool> movable= new Dictionary<Sens,bool> ();

	private SpriteRenderer renderer;
	// Use this for initialization
	void Start () {
		type = TypeElement.PLAYER;
		Affichage ();
		renderer = GetComponent<SpriteRenderer>();

		renderer.material.SetColor("_Color", Color.yellow);
		movable.Add (Sens.DROITE, true);
		movable.Add (Sens.GAUCHE, true);
		movable.Add (Sens.HAUT, true);
		movable.Add (Sens.BAS, true);
		movable.Add (Sens.NONE, true);
	}


	public void Affichage()
	{
		transform.position = new Vector2(PositionX, PositionY);
	}

		
}
