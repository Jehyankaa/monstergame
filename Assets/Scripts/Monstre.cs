﻿using UnityEngine;
using System.Collections;
using System.Runtime.Serialization;

///// <summary>
///// Action Possible de l'ennemi
///// </summary>
//public enum ActiontMonstre { STOP,MOUVEMENT,BLOQUER}
public enum Couleurs
{
    ALEATOIRE,
    ROUGE =0,
    VERT =1,
    BLEU =2,
    JAUNE =3}

public class Monstre:Element
{
    //public ActiontMonstre Action = ActiontMonstre.STOP;

    private static Color[] tabCouleur = { Color.red, Color.green, Color.blue, Color.yellow };

    public Couleurs Couleur = Couleurs.ALEATOIRE; 

    private SpriteRenderer renderer;

    /// <summary>
    /// Initialisation du monstre
    /// </summary>
    void Start()
    {

		type = TypeElement.MONSTER;
        renderer = GetComponent<SpriteRenderer>();

        Color tempCouleur = Color.white;

        if (Couleur == Couleurs.ALEATOIRE)
            tempCouleur = tabCouleur[Service.Instance.Rnd.Next(0, 3)];
        else
            tempCouleur = tabCouleur[(int)Couleur];

        renderer.material.SetColor("_Color", tempCouleur);
    }

	void Awake () {
		PositionX = transform.position.x;
		PositionY = transform.position.y;
		transform.position = new Vector2(PositionX, PositionY);

		//Resize 
		transform.localScale = new Vector3(2,2,0);
		//transform.localScale += new Vector3(1,1,0);
	}

    
}
