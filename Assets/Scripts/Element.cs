﻿using UnityEngine;
using System.Collections;

public class Element : MonoBehaviour {
	public float PositionX = 1f;
	public float PositionY = 1f;
	public int currentColumn=0;
	public int currentLine=0;

	public TypeElement type;


	// Appeller avant le chargement des methodes start
	void Awake () {
		PositionX = transform.position.x;
		PositionY = transform.position.y;
		transform.position = new Vector2(PositionX, PositionY);
	}
		
	public Case getNextCase(Sens sens, Case[,] tab){
		Case c = null;
		switch (sens) {
		case Sens.HAUT:
			if (currentLine + 1 < tab.GetLength (1))
				c = tab [currentLine + 1, currentColumn];
			break;
		case Sens.BAS:
			if (currentLine - 1 >= 0)
				c = tab [currentLine - 1, currentColumn];
			break;
		case Sens.DROITE:
			if (currentColumn + 1 < tab.GetLength (0))
				c = tab [currentLine, currentColumn + 1];
			break;
		case Sens.GAUCHE:
			if (currentColumn - 1 >= 0)
				c = tab [currentLine, currentColumn - 1];
			break;
		}
		return c;
	}

	public void setInCase(Case[,] tab, int i,int j){
		//i = line, j=colonne
		Case c = tab [i, j];
		PositionX = c.X;
		PositionY = c.Y;
		this.currentColumn = j;
		this.currentLine = i;
		transform.position = new Vector2(PositionX, PositionY);
	}
		


}
