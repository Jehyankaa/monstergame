﻿public class Service
{
    /// <summary>
    /// Objet service qui est static pour étre disponible partout
    /// </summary>
    private static Service instance;
    /// <summary>
    /// Variable qui permet de bloquer les autres appels le temps de l'instanciation (évite le problème en multiThead)
    /// </summary>
    private static object verrou = new System.Object();

    /// <summary>
    /// Assesseur à l'object random
    /// </summary>
    public System.Random Rnd { get; private set; }

    /// <summary>
    /// Instancie qu'une seule fois l'objet Service 
    /// </summary>
    /// <value>
    /// Retour l'objet Service
    /// </value>
    /// <remarks>
    /// Design parttern singleton, ne se charge en mémoire qu'une seul fois
    /// </remarks>
    public static Service Instance
    {
        get
        {
            if (instance == null)
            {
                lock (verrou) // verrou pour le multithead
                {
                    if (instance == null) // dernier verification (obligatoire selon microsoft), garantie que personne ne modifie l'objet
                        instance = new Service();
                }
            }

            return instance;
        }
    }

    /// <summary>
    /// Construiteur privé
    /// </summary>
    /// <remarks>
    /// Initialisation des objets public comme Rnd,...
    /// </remarks>
    private Service()
    {
        Rnd = new System.Random();
    }
}


