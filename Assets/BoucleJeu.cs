﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

using UnityEditor;

public enum Sens
{
	DROITE, GAUCHE, HAUT, BAS,NONE
}

public enum TypeElement
{
	CUBE, PLAYER, MONSTER, NONE
}

public class Node
{
	public Case c { get;  set; }
	public bool visité { get;  set; }
	public int colonne{ get;  set; }
	public int ligne { get;  set; }

	public Node c_pere { get; set; }
}

public class Case
{
    public float X { get;  set; }
    public float Y { get;  set; }
    public bool Utiliser { get; set; }
	public Element element { get; set; }
}

public class BoucleJeu : MonoBehaviour {

	// Quand difficulté = 400, la mise a jour se fait environ toutes les 5 secondes
	private static int DIFFICULTE = 400;
	private static int FREQUENCE_DEPLACEMENT = 100;

	public static int GAME_SIZE = 12;
	public static float VELOCITY;
	public static float INTERVALLE = 0.05f;

	public float screen_XMin, screen_YMin, screen_XMax, screen_YMax;
    public float limite_XMin, limite_YMin, limite_XMax, limite_YMax;  // nouvelle limite - car c'est le tableau tab_cases qui est la nouvelle limite
    public float cube_width, cube_height;
   
	public Sens s;

	// compteur permettant l'intervalle entre deux calcul de parcours en largeur
	private int ia_count = 0;
	private int ia_deplacements_count = 0;
	private Stack chemin_monstre= new Stack();

	public Player player;
	public Cube [] cubes_go;
	public Monstre monstre;

	public Node[,] tab_nodes =new Node[GAME_SIZE, GAME_SIZE];
	public Case[,] tab_cases = new Case[GAME_SIZE, GAME_SIZE];
	//Case temporaire vérifiant la disponibilité des prochains
	public Case c,c2;

    

	void ParcoursLargeur(Node depart, Node player){
		Debug.Log ("Parcours en largeur en cours");
		System.Object[] array;
		resetNodes();
		Queue file = new Queue ();
		file.Enqueue (depart);
		depart.visité = true;

		//On descend jusqu'au player
		while (depart!=player) {
			
			depart = (Node)file.Dequeue ();

			Node[] nodes = getVoisins (depart.ligne, depart.colonne);
			for (int i = 0; i < 4; i++) {
				if (nodes[i] != null && !nodes [i].visité) {

					tab_nodes [nodes [i].ligne, nodes [i].colonne].visité = true;
					tab_nodes [nodes[i].ligne, nodes [i].colonne].c_pere = depart;
				
					if (!nodes [i].c.Utiliser || nodes[i]==player) 
						file.Enqueue (nodes [i]);

				}
			}
		}



	
		//On remonte pour garder le chemin en mémoire 
		while (depart != tab_nodes [monstre.currentLine, monstre.currentColumn]) {
			chemin_monstre.Push (depart);
			depart = depart.c_pere;
		
		}
			
	}



    void Start () {
		

		//Définit les limites de l'écran
		screen_XMin = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, Camera.main.transform.position.z)).x;
		screen_XMax = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, Camera.main.transform.position.z)).x;
		screen_YMin = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, Camera.main.transform.position.z)).y;
		screen_YMax = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, Camera.main.transform.position.z)).y;
		//******************************

		//screen_XMax = GAME_SIZE * cube_width;
		//screen_YMax = screen_XMax;
		cube_height = cubes_go[0].GetComponent<Renderer> ().bounds.size.y;
		cube_width = cubes_go[0].GetComponent<Renderer> ().bounds.size.x;


        setCells();
		initNodes ();
		// Je comprends pas l'utilité
        /*tab_cases[GAME_SIZE - 2, 0].Utiliser = true;
        tab_cases[GAME_SIZE - 2, 1].Utiliser = true;
        tab_cases[GAME_SIZE - 2, 2].Utiliser = true;
        tab_cases[GAME_SIZE - 2, 3].Utiliser = true;

        tab_cases[0, 0].Utiliser = true;*/

        float bonhommePosX = tab_cases[0, 1].X;
        float bonhommePosY = tab_cases[0, 1].Y;
	

        /*tab_cases[0, 2].Utiliser = true;
        tab_cases[0, 3].Utiliser = true;*/

        // Définir le limite du tableau tab_cases
        limite_XMin = tab_cases[0,0].X - ((cube_width+0.001f)/2f);
        limite_YMin = tab_cases[0, 0].Y - ((cube_width + 0.001f) / 2f);
        limite_XMax = tab_cases[GAME_SIZE - 1, GAME_SIZE - 1].X + (cube_width);
        limite_YMax = tab_cases[GAME_SIZE - 1, GAME_SIZE - 1].Y + (cube_height);


        placementCubes();
		
		VELOCITY = cube_width;

		moveToCase(0,1,player);
		moveToCase (3, 3, monstre);
	
        //player.transform.position = new Vector2(bonhommePosX, bonhommePosY);
    }

	// Update is called once per frame
	void Update () {

		//Le joueur pourrait aussi être bloqué dans une version ultérieure (partie à deux etc..)
		if (isBlocked (monstre)) 
			endGame("Monstre a perdu, Vous avez gagné Bravoo tu déchires You are The Best You rooooock it !");
		if (isBlocked (player) || monstreMangePlayer()) 
			endGame("Tu as perdu, t'es nulle tu crains !!");


		if (ia_count < DIFFICULTE)
			ia_count++;
		else {
			ia_count = 0;
			ParcoursLargeur (tab_nodes [monstre.currentLine, monstre.currentColumn],tab_nodes[player.currentLine,player.currentColumn]);
		}

		if (ia_deplacements_count < FREQUENCE_DEPLACEMENT)
			ia_deplacements_count++;
		else {
			ia_deplacements_count = 0;
			if(chemin_monstre.Count>0){
				Node n = (Node)chemin_monstre.Pop ();
				moveToCase (n.ligne,n.colonne, monstre);
			}
		}




		/*************************************************** DEPLACEMENTS MONSTRES **************************************************/
		/*if (Input.GetKeyDown(KeyCode.Z))
		{
			c =monstre.getNextCase (Sens.HAUT, tab_cases);

			if (c != null&&!c.Utiliser) 
				moveToCase (monstre.currentLine + 1, monstre.currentColumn, monstre);	
			
		}
		else if (Input.GetKeyDown(KeyCode.S))
		{
			c =monstre.getNextCase (Sens.BAS, tab_cases);

			if (c != null&&!c.Utiliser) 
				moveToCase (monstre.currentLine - 1, monstre.currentColumn, monstre);	
		}
		else if (Input.GetKeyDown(KeyCode.Q))
		{
			c =monstre.getNextCase (Sens.GAUCHE, tab_cases);

			if (c != null&&!c.Utiliser) 
				moveToCase (monstre.currentLine, monstre.currentColumn - 1, monstre);	
		}
		else if (Input.GetKeyDown(KeyCode.D))
		{
			c =monstre.getNextCase (Sens.DROITE, tab_cases);

			if (c != null&&!c.Utiliser) 
				moveToCase (monstre.currentLine, monstre.currentColumn +1, monstre);	
		}
		/*************************************************** -------- **************************************************
		*/

		/*************************************************** DEPLACEMENTS PLAYER **************************************************/
		if (Input.GetKeyDown (KeyCode.UpArrow)) {
			c =player.getNextCase (Sens.HAUT, tab_cases);
			if (c != null) {
				if (c.Utiliser) {
					c2 =c.element.getNextCase (Sens.HAUT, tab_cases);
					if (c2 != null && !c2.Utiliser) {
						if(c.element.type!=TypeElement.MONSTER)
							moveToCase (c.element.currentLine + 1, c.element.currentColumn, c.element);
						moveToCase (player.currentLine + 1, player.currentColumn, player);	

					}

				}
				else
					moveToCase (player.currentLine + 1, player.currentColumn, player);	
			}
		} else if (Input.GetKeyDown (KeyCode.DownArrow)) {
			c =player.getNextCase (Sens.BAS, tab_cases);
			if (c != null ) {
				if (c.Utiliser){
					c2 =c.element.getNextCase (Sens.BAS, tab_cases);
					if (c2 != null && !c2.Utiliser) {
						if(c.element.type!=TypeElement.MONSTER)
							moveToCase (c.element.currentLine - 1, c.element.currentColumn, c.element);
						moveToCase (player.currentLine - 1, player.currentColumn, player);	

					}

				}
				else
					moveToCase (player.currentLine - 1, player.currentColumn, player);
			}

		} else if (Input.GetKeyDown (KeyCode.LeftArrow)) {
			c =player.getNextCase (Sens.GAUCHE, tab_cases);
			if (c != null) {
				if (c.Utiliser){
					c2 =c.element.getNextCase (Sens.GAUCHE, tab_cases);
					if (c2 != null && !c2.Utiliser) {
						if(c.element.type!=TypeElement.MONSTER)
							moveToCase (c.element.currentLine, c.element.currentColumn - 1, c.element);
						moveToCase (player.currentLine, player.currentColumn - 1, player);

					}

				}
				else
					moveToCase (player.currentLine, player.currentColumn - 1, player);
			}
			
		} else if (Input.GetKeyDown (KeyCode.RightArrow)) {
			c =player.getNextCase (Sens.DROITE, tab_cases);
			if (c != null) {
				if (c.Utiliser){
					c2 =c.element.getNextCase (Sens.DROITE, tab_cases);
					if (c2 != null && !c2.Utiliser) {
						if(c.element.type!=TypeElement.MONSTER)
							moveToCase (c.element.currentLine, c.element.currentColumn + 1, c.element);
						moveToCase (player.currentLine, player.currentColumn + 1, player);

					}

				}
				else
					moveToCase (player.currentLine, player.currentColumn +1, player);
			}
		}
		/*************************************************** ----- **************************************************/
		debugGrille ();
       

    }

	void resetNodes(){
		for (int i = 0; i < GAME_SIZE; i++) {
			for (int j = 0; j < GAME_SIZE; j++) {
				tab_nodes [i, j].visité = false;
			}
		}
		chemin_monstre.Clear ();
	}
	void initNodes(){
		for (int i = 0; i < GAME_SIZE; i++) {
			for (int j = 0; j < GAME_SIZE; j++) {
				tab_nodes [i, j] = new Node {c=tab_cases [i, j], colonne = j,ligne = i, visité=false, c_pere=null} ;

			}
		}
	}

	public Node[] getVoisins(int i, int j){

		Node c1= null, c2=null, c3=null, c4=null;
		if (i + 1 < tab_cases.GetLength (1))
			c1 = tab_nodes [i + 1, j];

		if (i - 1 >= 0)
			c2 = tab_nodes [i - 1, j];
		if (j + 1 < tab_cases.GetLength (0))
			c3 = tab_nodes [i, j + 1];
		if (j - 1 >= 0)
			c4 = tab_nodes [i, j - 1];
		Node[] tab = {c1,c2,c3,c4};
		return tab;

	}

		
	private void setCells()
	{


		float factor = cube_width;

		// pour centré, on calcul la valeur minimuin sur le bas à gauche
		float positionMinX = screen_XMin + (((screen_XMax * 2f)- ((GAME_SIZE + 1f) * factor)) / 2f);
		float positionMinY = screen_YMin + (((screen_YMax * 2f) - ((GAME_SIZE -1f) * factor)) / 2f); // moins car c'est pas le même sens dans l'alimentation du tableau
		// fin centré(float)

		float tempY = positionMinY;

		for (int j = 0; j < GAME_SIZE; j++)
		{

			float tempX = positionMinX;

			for (int i = 0; i < GAME_SIZE; i++)
			{
				tempX += factor;

				tab_cases[j, i] = new Case() { X = tempX, Y = tempY , Utiliser = false };
			}

			tempY += factor;
		}
	}






		
	public void debugHitBox(float x, float y,float width, float height, Color col)
	{
		Vector3 a = new Vector3 (x, y, 0);
		Vector3 b = new Vector3 (x+width, y, 0);
		Vector3 c = new Vector3 (x, y+height, 0);
		Vector3 d = new Vector3 (x+width, y+height, 0);
		Debug.DrawLine(a, b, col);
		Debug.DrawLine(a, c, col);
		Debug.DrawLine(b, d, col);
		Debug.DrawLine(c, d, col);
	}


	public void placementCubes(){


        System.Random rnd = new System.Random();

        int x , y;

        for (int i = 0; i<cubes_go.Length; i++) {

            do
            {
                x = rnd.Next(0, GAME_SIZE);
                y = rnd.Next(0, GAME_SIZE);

            } while (tab_cases[y, x].Utiliser);

			moveToCase (y, x, cubes_go [i]);

		}
	}

    private void debugGrille()
    {
        for (int x = 0; x < GAME_SIZE; x++)
        {
            Debug.DrawLine(new Vector2(tab_cases[0, x].X-(cube_width/2f), tab_cases[0, x].Y - (cube_width / 2f)), new Vector2(tab_cases[GAME_SIZE-1, x].X  - (cube_width / 2f), tab_cases[GAME_SIZE-1, x].Y + cube_width - (cube_width / 2f)), Color.white);

            if (x == (GAME_SIZE - 1)) // finir le carré :)
                Debug.DrawLine(new Vector2(tab_cases[0, x].X + cube_width - (cube_width / 2f), tab_cases[0, x].Y - (cube_width / 2f)), new Vector2(tab_cases[GAME_SIZE - 1, x].X + cube_width - (cube_width / 2f), tab_cases[GAME_SIZE - 1, x].Y + cube_width - (cube_width / 2f)), Color.white);
        }

        for (int y = 0; y < GAME_SIZE; y++)
        {
            Debug.DrawLine(new Vector2(tab_cases[y, 0].X - (cube_width / 2f), tab_cases[y, 0].Y - (cube_width / 2f)), new Vector2(tab_cases[y,GAME_SIZE - 1].X + cube_width - (cube_width / 2f), tab_cases[y,GAME_SIZE - 1].Y - (cube_width / 2f)), Color.white);

            if (y == (GAME_SIZE-1)) // finir le carré :)
                Debug.DrawLine(new Vector2(tab_cases[y, 0].X - (cube_width / 2f), tab_cases[y, 0].Y + cube_width - (cube_width / 2f)), new Vector2(tab_cases[y, GAME_SIZE - 1].X + cube_width - (cube_width / 2f), tab_cases[y, GAME_SIZE - 1].Y + cube_width - (cube_width / 2f)), Color.white);
        }
        
    }

	private void moveToCase(int i, int j, Element element){
		if (!tab_cases [i, j].Utiliser) {
			tab_cases [element.currentLine, element.currentColumn].Utiliser = false;
			element.setInCase (tab_cases, i, j);
			tab_cases [i, j].Utiliser = true;
			tab_cases [i, j].element = element;
		}
	}

	private bool monstreMangePlayer(){
		return((monstre.currentLine == player.currentLine) && (monstre.currentColumn == player.currentColumn));
	}


	//Returns true if the element can't move anymore
	private bool isBlocked(Element element){
		Case c1, c2, c3, c4;
		c1 = element.getNextCase (Sens.HAUT, tab_cases);
		c2 = element.getNextCase (Sens.BAS, tab_cases);
		c3 = element.getNextCase (Sens.DROITE, tab_cases);
		c4 = element.getNextCase (Sens.GAUCHE, tab_cases);

		return ((c1 == null || c1.Utiliser) && (c2 == null || c2.Utiliser) && (c3 == null || c3.Utiliser) && (c4 == null || c4.Utiliser));
	}




	void endGame(String msg){

		bool rep =EditorUtility.DisplayDialog ("Fin du jeu", msg, "Relancer", "Fermer");
		if (rep)
			UnityEngine.SceneManagement.SceneManager.LoadScene (1);
		else
			UnityEngine.SceneManagement.SceneManager.LoadScene(0);
	}


	/*/******** INUTILE **


	public bool isInScreen(float posX, float posY)
	{
		//return (posX > screen_XMin) && (posX < screen_XMax) && (posY > screen_YMin) && (posY < screen_YMax);
		return (posX > limite_XMin) && (posX < limite_XMax) && (posY > limite_YMin) && (posY < limite_YMax);  // pour les limite du tableau
	}

	private float convertCenterPointX (float x, float width){
		float res = x - width / 2f; // ne pas oublier le f car sinon c'est un int pour lui et cela fera de petite erreur de calcul 
		return res;
	}

	private float convertCenterPointY (float y, float height){
		float res = y - height / 2f;
		return res;
	}

	*/


}
